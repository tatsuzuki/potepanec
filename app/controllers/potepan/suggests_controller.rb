# frozen_string_literal: true

class Potepan::SuggestsController < ApplicationController
  MAX_SUGGESTS_COUNT = 5
  def index
    response = ApiSuggest.get_suggest_response(params[:keyword], MAX_SUGGESTS_COUNT)
    suggest = response.body
    if response.status == 200
      render json: suggest
    else
      render status: 500, json: suggest
    end
  end
end
