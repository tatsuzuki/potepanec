# frozen_string_literal: true

require 'httpclient'

class ApiSuggest
  API_KEY = Rails.application.credentials.api[:POTEPAN_API_KEY]
  POTEPAN_API_URI = Rails.application.credentials.api[:POTEPAN_API_URL]

  def self.get_suggest_response(keyword, max_num)
    headers = {
      Authorization: "Bearer #{API_KEY}"
    }
    params = {
      keyword: keyword,
      max_num: max_num
    }
    client = HTTPClient.new
    response = client.get(POTEPAN_API_URI, header: headers, body: params)
    response
  end
end
