# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Potepan::CategoriesController, type: :controller do
  describe 'GET #show' do
    let(:taxonomy) { create(:taxonomy) }
    let(:taxon) { create(:taxon, taxonomy: taxonomy) }
    let!(:product) { create(:product, taxons: [taxon]) }

    before do
      get :show, params: { id: taxon.id }
    end

    it 'リクエストが成功すること' do
      expect(response).to have_http_status(:success)
    end

    it 'showテンプレートで表示されること' do
      expect(response).to render_template :show
    end

    it '@taxonが取得できていること' do
      expect(assigns(:taxon)).to eq taxon
    end

    it '@taxonomiesが取得できていること' do
      expect(assigns(:taxonomies).first).to eq taxonomy
    end

    it '@productsが取得できていること' do
      expect(assigns(:products)).to eq taxon.all_products
    end
  end
end
