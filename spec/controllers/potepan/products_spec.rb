# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Potepan::ProductsController, type: :controller do
  describe 'GET #show' do
    let(:taxonomy) { create(:taxonomy) }
    let(:taxon) { create(:taxon, taxonomy: taxonomy) }
    let(:product) { create(:product, taxons: [taxon]) }
    let!(:related_products) { create_list(:product, 4, taxons: [taxon]) }

    before do
      get :show, params: { id: product.id }
    end

    it 'リクエストが成功すること' do
      expect(response).to have_http_status(:success)
    end

    it 'showテンプレートで表示されること' do
      expect(response).to render_template :show
    end

    it '@productが取得できていること' do
      expect(assigns(:product)).to eq product
    end

    it '@related_productsが取得できていること' do
      expect(assigns(:related_products)).to eq related_products
    end
  end
end
