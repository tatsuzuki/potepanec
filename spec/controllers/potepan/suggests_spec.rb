# frozen_string_literal: true

require 'rails_helper'
require 'webmock/rspec'
require 'json'

RSpec.describe Potepan::SuggestsController, type: :controller do
  context 'リクエストが成功した場合' do
    before do
      stub_request(:get, Rails.application.credentials.api[:POTEPAN_API_URL])
        .with(
          headers: { Authorization: "Bearer #{Rails.application.credentials.api[:POTEPAN_API_KEY]}" },
          body: { 'keyword' => 'rails', 'max_num' => '5' }
        ).to_return(
          status: 200,
          headers: { 'Content-Type' => 'application/json' },
          body: ['rails', 'rails for men', 'rails for women']
        )
      get :index, params: { keyword: 'rails' }
    end

    it 'ステータスコードが200になっていること' do
      expect(response).to have_http_status 200
    end

    it 'responseにSuggestする項目が含まれていること' do
      expect(JSON.parse(response.body)).to eq ['rails', 'rails for men', 'rails for women']
    end
  end

  context 'リクエストが失敗した場合' do
    before do
      stub_request(:get, Rails.application.credentials.api[:POTEPAN_API_URL])
        .with(
          headers: { Authorization: "Bearer #{Rails.application.credentials.api[:POTEPAN_API_KEY]}" },
          body: { 'keyword' => 'rails', 'max_num' => '5' }
        ).to_return(
          status: 500,
          headers: { 'Content-Type' => 'application/json' },
          body: ''
        )
      get :index, params: { keyword: 'rails' }
    end

    it 'ステータスコードが500になっていること' do
      expect(response).to have_http_status 500
    end
  end
end
