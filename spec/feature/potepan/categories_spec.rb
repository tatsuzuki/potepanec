# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Categories feature', type: :feature do
  let(:taxonomy) { create(:taxonomy) }
  let(:taxon) { create(:taxon, parent_id: taxonomy.root.id, taxonomy: taxonomy) }
  let(:other_taxon) { create(:taxon, name: 'other_taxon', parent_id: taxonomy.root.id, taxonomy: taxonomy) }
  let!(:product) { create(:product, taxons: [taxon]) }
  let!(:other_product) { create(:product, name: 'other_product', taxons: [other_taxon]) }

  before do
    visit potepan_category_path(taxon.id)
  end

  it 'サイドバーのtaxonクリックで、該当taxonページにジャンプすること' do
    click_on "#{taxon.name} (#{taxon.products.count})"
    expect(current_path).to eq potepan_category_path(taxon.id)
  end

  it 'taxonomyとtaxon、taxonに紐づくproduct情報が表示されること。また他のtaxonに紐づくproductが表示されないこと' do
    expect(page).to have_content taxonomy.name
    expect(page).to have_content "#{taxon.name} (#{taxon.products.count})"
    expect(page).to have_content taxon.name
    expect(page).to have_content product.name
    expect(page).to have_content product.display_price
    expect(page).not_to have_content other_product.name
  end

  it 'Productページへジャンプし、Catecoryページに戻ること' do
    click_on product.name
    expect(current_path).to eq potepan_product_path(product.id)
    click_on '一覧ページへ戻る'
    expect(current_path).to eq potepan_category_path(taxon.id)
  end
end
