# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Product feature', type: :feature do
  let(:taxon) { create(:taxon) }
  let(:other_taxon) { create(:taxon, name: 'other_taxon') }
  let!(:product) { create(:product, taxons: [taxon]) }
  let!(:related_products) { create_list(:product, 4, taxons: [taxon]) }
  let!(:other_products) { create_list(:product, 5, name: 'other_product', taxons: [other_taxon]) }

  before do
    visit potepan_product_path(product.id)
  end

  it '商品名、価格、紹介文、関連商品が表示されており、かつ別taxonに紐づく商品は関連商品に表示されないこと' do
    expect(page).to have_content product.name
    expect(page).to have_content product.display_price
    expect(page).to have_content product.description
    expect(page).to have_content related_products.first.name
    expect(page).to have_content related_products.first.display_price
    expect(page).to have_selector '.productBox', count: 4
    expect(page).not_to have_content other_products.first.name
  end

  it '関連商品の表示上限が４件になっていること' do
    visit potepan_product_path(other_products.first.id)
    expect(page).to have_selector '.productBox', count: 4
  end

  it '関連商品クリックで、その商品ページにジャンプすること' do
    click_on related_products.first.name
    expect(current_path).to eq potepan_product_path(related_products.first.id)
  end

  it 'タイトルが正しく表示されていること' do
    expect(page).to have_title "#{product.name} - BIGBAG Store"
  end

  it 'Homeに正しく戻ること' do
    click_on 'home-logo'
    expect(page).to have_title 'BIGBAG Store'
  end
end
