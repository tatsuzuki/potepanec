# frozen_string_literal: true

require 'rails_helper'

RSpec.describe ApplicationHelper, type: :helpers do
  describe '#full_title' do
    include ApplicationHelper
    context 'titleが指定されていない場合' do
      it 'デフォルトタイトルが返されること' do
        expect(full_title).to eq 'BIGBAG Store'
      end
    end

    context '引数にnilが渡ってきた場合' do
      it 'デフォルトタイトルが返されること' do
        expect(full_title(nil)).to eq 'BIGBAG Store'
      end
    end

    context 'titleが指定されている場合' do
      let(:product) { create(:product) }
      it '商品名が入ったタイトルが返されること' do
        expect(full_title(product.name)).to eq "#{product.name} - BIGBAG Store"
      end
    end
  end
end
