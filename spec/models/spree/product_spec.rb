# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Spree::Product, type: :model do
  describe 'related_products' do
    let(:taxonomy) { create :taxonomy }
    let(:taxon) { create :taxon, taxonomy: taxonomy }
    let(:product) { create :product, taxons: [taxon] }
    let!(:related_products) { create_list(:product, 4, taxons: [taxon]) }
    let(:other_taxon) { create :taxon }
    let(:not_related_product) { create :product, taxons: [other_taxon] }

    it '関連商品を取得できること' do
      expect(product.related_products).to eq related_products
    end
  end
end
